echo off
echo Starting tomcat container, can take approximately 1 minute to complete, please wait...
docker run -p 8090:8080 -dit --name t9s-dbda-90 tomcat9-server
timeout 30 /NOBREAK

docker cp dbanalyzer.war  t9s-dbda-90:/usr/local/tomcat/webapps

echo ..
echo ..
echo tomcat should now be available
echo
